package com.psys.gitinfov2;

import android.app.Application;
import android.content.Context;

import com.psys.gitinfov2.support.HttpRequestHandler;
import com.psys.gitinfov2.support.LoggingInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class BaseApp extends Application {

    private static OkHttpClient okHttpClient;
    private static Context appContext;

    /**
     * @return OkHttpClient Application level instance ,to share cache and interceptor
     */
    public static OkHttpClient getOkHttpClient(LoggingInterceptor interceptor) {
        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(10 * 1000, TimeUnit.MILLISECONDS)
                    .connectTimeout(10 * 1000, TimeUnit.MILLISECONDS)
                    .addInterceptor(interceptor)
                    .build();
        }
        return okHttpClient;
    }

    public static Context getAppContext() {
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        okHttpClient = getOkHttpClient(HttpRequestHandler.getInstance());
        appContext = getApplicationContext();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}

package com.psys.gitinfov2.support;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.psys.gitinfov2.BaseApp;

import java.io.IOException;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public final class HttpRequestHandler extends LoggingInterceptor {
    private final OkHttpClient client;
    private static HttpRequestHandler httpRequestHandler;
    private final String TAG = HttpRequestHandler.class.getSimpleName();

   /* @Deprecated
    private HttpRequestHandler() {
        client = new OkHttpClient.Builder()
                .readTimeout(10 * 1000, TimeUnit.MILLISECONDS)
                .connectTimeout(10 * 1000, TimeUnit.MILLISECONDS)
                .addInterceptor(this)
                .build();
    }*/

    private HttpRequestHandler() {
        client = BaseApp.getOkHttpClient(this);
    }

    public static HttpRequestHandler getInstance() {
        if (httpRequestHandler == null) {
            httpRequestHandler = new HttpRequestHandler();
        }
        return httpRequestHandler;
    }

    /**
     * @see <b>@Method 'processGet()' and similar pattern function.</b>
     * @deprecated method access will be converted into private in future release.
     */
    @Deprecated
    public HttpUrl prepareLink(@NonNull String pathSegment, @Nullable Map<String, String> queryParams) {
        if (pathSegment == null) {

        }
        HttpUrl.Builder urlPattern = new HttpUrl.Builder();
        urlPattern.scheme("https");
        urlPattern.host("api.github.com");
        urlPattern.addPathSegments(pathSegment);
        for (String key : queryParams.keySet()) {
            urlPattern.addQueryParameter(key, queryParams.get(key));
        }
        return urlPattern.build();
    }


    public void processGet(HttpUrl sourceLink, final ResponseHandler responseHandler) {

        if (responseHandler == null) {
            throw new IllegalArgumentException(" ResponseHandler should not be null!");
        } else if (!isNetworkAvailable(BaseApp.getAppContext())) {
            responseHandler.onResponseError("Network Issue, network not available.",
                    new IOException("Not able to connect with server, possibly internet Problem. "));
        }
        Request request = new Request.Builder()
                .url(sourceLink.url())
                .get()
                .build();
        client.newCall(request).enqueue(new Callback() {
                                            @Override
                                            public void onFailure(Call call, IOException e) {
                                                Log.e(TAG, "onFailure: ", e);
                                                responseHandler.onResponseError(e.getMessage(), e);
                                            }

                                            @Override
                                            public void onResponse(Call call, Response response) throws IOException {
                                                if (response.isSuccessful()) {
                                                    String headerString = response.header("Link");
                                                    responseHandler.onResponseSuccess(response.body(), extractUrlFromLink(headerString));
                                                } else {
                                                    responseHandler.onResponseError("Request is not successfully executed code : " +
                                                            response.code(), new IOException(" Request malfunctioned!"));
                                                }
                                            }

                                            private String[] extractUrlFromLink(String linkString) {
                                                Log.d(TAG, "extractUrlFromLink : " + linkString);
                                                String[] linkUrls = new String[2];
                                                try {
                                                    String[] splitLink = linkString.split(",");
                                                    if (splitLink[0].contains("next")) {
                                                        linkUrls[0] = splitLink[0].substring(splitLink[0].indexOf("<") + 1, splitLink[0].indexOf(">"));
                                                    } else if (splitLink[1].contains("last")) {
                                                        linkUrls[1] = splitLink[1].substring(splitLink[1].indexOf("<") + 1, splitLink[1].indexOf(">"));
                                                    } else if (splitLink[1].contains("first")) {
                                                        linkUrls[1] = splitLink[1].substring(splitLink[1].indexOf("<") + 1, splitLink[1].indexOf(">"));
                                                    }
                                                } catch (NullPointerException e) {
                                                    Log.e(TAG, "extractUrlFromLinkHeader: ", e);
                                                }
                                                return linkUrls;
                                            }
                                        }
        );
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Log.e(TAG, "intercept: init--------------");
        return super.intercept(chain);
    }

    /**
     * Check Network availability bases of network interfaces.
     *
     * @param con is Context present.
     */
    public boolean isNetworkAvailable(Context con) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) con
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (NullPointerException e) {
            Log.e(TAG, "isNetworkAvailable: Networking interface is null!", e);
            return false;
        }
    }

    public String clipExtraUrl(String urlStr) {
        String tempUrl = urlStr;
        Log.d(TAG, "clipExtraUrl: INPUT-STRING " + urlStr);
        if (tempUrl.contains("{") || tempUrl.contains("}")) {
            urlStr = tempUrl.replaceAll("\\{.*\\}", "");
        } else {
            // do nothing to url
        }
        Log.d(TAG, "clipExtraUrl: OUTPUT-STRING " + urlStr);
        return urlStr;
    }

    public interface ResponseHandler {

        /**
         * <p>Provide result of Request</p>
         *
         * @param response           is content body of response on successful execution of request.
         * @param nextLastOrFirstUrl is array of link URL for pagination support received in header.
         * @throws IOException caught when unexpected event like socket/Connection issue.
         */
        void onResponseSuccess(ResponseBody response, @Nullable String[] nextLastOrFirstUrl) throws IOException;

        /**
         * <p>Provides exception during Request</p>
         *
         * @param errorMessage error message or cause.
         * @throws IOException caught when unexpected event like socket/Connection issue.
         */

        void onResponseError(String errorMessage, IOException e);

    }
}

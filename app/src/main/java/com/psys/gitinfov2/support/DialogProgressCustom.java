package com.psys.gitinfov2.support;

/**
 * Created by beau on 9/6/2016.
 */

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.psys.gitinfov2.R;


@SuppressLint("InflateParams")
public class DialogProgressCustom {
    private static Dialog mDialog = null;
    private static DialogProgressCustom customDialog = null;
    String TAG = DialogProgressCustom.class.getSimpleName();

    private DialogProgressCustom() {
        // private constructor
    }

    public static DialogProgressCustom getInstance() {
        if (customDialog == null) {
            customDialog = new DialogProgressCustom();
        }
        return customDialog;
    }

    public void startProgressDialog(Context mContext, boolean cancellable) throws WorkerException {
        // check for UI thread
        if (Looper.myLooper() != Looper.getMainLooper()) {
            throw new WorkerException("Progress indicator not UI thread.");
        }
        stopProgressDialog();
        try {
            mDialog = new Dialog(mContext, R.style.ProgressDialogStyle);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(mContext, R.color.colorTransparent)));
            LayoutInflater mInflater = LayoutInflater.from(mContext);
            View layout = mInflater.inflate(R.layout.dialog_progress_custom, null);
            mDialog.setContentView(layout);
            mDialog.setCancelable(cancellable);
            mDialog.show();
        } catch (NullPointerException e) {
            Log.e(TAG, "startProgressDialog: ", e);
        }
    }

    /**
     * Functionality to stop progress dialog if running
     */
    public void stopProgressDialog() {
        try {
            if (mDialog != null) {
                mDialog.dismiss();
            }
            mDialog = null;
        } catch (Exception e) {
            Log.e(TAG, "stopProgressDialog: Exception By : " + e.getClass().getSimpleName(), e);
        }

    }

}

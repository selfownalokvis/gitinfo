package com.psys.gitinfov2.support;

import android.os.Build;
import android.support.annotation.RequiresApi;

public class NetworkExceptions extends Exception {
    public NetworkExceptions() {
    }

    public NetworkExceptions(String message) {
        super(message);
    }

    public NetworkExceptions(String message, Throwable cause) {
        super(message, cause);
    }

    public NetworkExceptions(Throwable cause) {
        super(cause);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public NetworkExceptions(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

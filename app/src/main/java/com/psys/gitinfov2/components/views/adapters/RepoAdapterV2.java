package com.psys.gitinfov2.components.views.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.psys.gitinfov2.R;
import com.psys.gitinfov2.components.views.beans.Repository;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RepoAdapterV2 extends RecyclerView.Adapter<RepoAdapterV2.MyViewHolder> {

    private final String TAG = getClass().getSimpleName();
    private List<Repository> repositories = new ArrayList<>();
    private int mExpandedPosition = -1;
    private OnRepoComponentSelectedListener repoComponentSelectedListener;

    public RepoAdapterV2(List<Repository> repositories) {
        this.repositories = repositories;

    }

    public RepoAdapterV2(List<Repository> repositories, OnRepoComponentSelectedListener repoComponentSelectionListener) {
        this.repositories = repositories;
        this.repoComponentSelectedListener = repoComponentSelectionListener;
    }


    public void setRepoComponentSelectionListener(OnRepoComponentSelectedListener repoComponentSelectionListener) {
        repoComponentSelectedListener = repoComponentSelectionListener;
    }

    @Override
    public RepoAdapterV2.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_list_row_layout, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RepoAdapterV2.MyViewHolder holder, final int position) {

        final int listPosition = holder.getAdapterPosition();

        Repository repository = getItem(listPosition);
        holder.tvRepoName.setText(repository.getRepoName());
        holder.tvRepoDesc.setText(repository.getRepoDescription());
        Picasso.get().load(repository.getOwnerImageUrlBySize(Repository.IMG_THUMB)).resize(72, 72).into(holder.ivOwnerPic);

        // ui effect operation start
        final boolean isExpanded = position == mExpandedPosition;
        holder.options.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.ivExpandMore.setActivated(isExpanded);
        holder.ivExpandMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1 : position;
                notifyItemChanged(position);
                v.setRotation(180f);
            }
        });

    }

    public Repository getItem(int index) {
        return repositories.get(index);
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvRepoName, tvRepoDesc;
        CardView cardView;
        ImageView ivExpandMore;
        ImageView ivOwnerPic;
        ImageView ivContributor;
        ImageView ivBranches;
        ImageView ivLanguages;
        LinearLayout options;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.tvRepoName = (TextView) itemView.findViewById(R.id.tvRepoName);
            this.tvRepoDesc = (TextView) itemView.findViewById(R.id.tvRepoDesc);
            this.cardView = (CardView) itemView.findViewById(R.id.cardexpolist);
            this.ivExpandMore = (ImageView) itemView.findViewById(R.id.ivRepoExpand);
            this.ivOwnerPic = (ImageView) itemView.findViewById(R.id.ivOwnerPic);
            this.ivContributor = (ImageView) itemView.findViewById(R.id.icContributor);
            this.ivBranches = (ImageView) itemView.findViewById(R.id.icBranches);
            this.ivLanguages = (ImageView) itemView.findViewById(R.id.icLanguages);
            this.options = (LinearLayout) itemView.findViewById(R.id.optionContainer);
            setListeners();
        }

        private void setListeners() {

            ivContributor.setOnClickListener(this);
            ivLanguages.setOnClickListener(this);
            ivBranches.setOnClickListener(this);
        }


        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            if (repoComponentSelectedListener == null) {
                throw new IllegalStateException("Must implement OnRepoComponentSelectedListener to access this services.");
            }
            switch (v.getId()) {
                case R.id.icBranches:
                    Log.i(TAG, "onClick: branch operation");
                    repoComponentSelectedListener.onBranchesSelected(getItem(getAdapterPosition()), getAdapterPosition());
                    break;

                case R.id.icContributor:
                    Log.i(TAG, "onClick: Contribute operation");
                    repoComponentSelectedListener.onContributorSelected(getItem(getAdapterPosition()), getAdapterPosition());
                    break;

                case R.id.icLanguages:
                    Log.i(TAG, "onClick: Language operation");
                    repoComponentSelectedListener.onLanguageSelected(getItem(getAdapterPosition()), getAdapterPosition());
                    break;
            }
        }
    }

    public interface OnRepoComponentSelectedListener {
        void onContributorSelected(Repository selectedRepo, int position);

        void onLanguageSelected(Repository selectedRepo, int position);

        void onBranchesSelected(Repository selectedRepo, int position);
    }


}

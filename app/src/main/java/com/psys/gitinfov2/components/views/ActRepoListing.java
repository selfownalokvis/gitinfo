package com.psys.gitinfov2.components.views;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.psys.gitinfov2.R;
import com.psys.gitinfov2.components.views.adapters.RepoAdapterV2;
import com.psys.gitinfov2.components.views.beans.Repository;
import com.psys.gitinfov2.support.DialogProgressCustom;
import com.psys.gitinfov2.support.HttpRequestHandler;
import com.psys.gitinfov2.support.WorkerException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.ResponseBody;

public class ActRepoListing extends AppCompatActivity implements HttpRequestHandler.ResponseHandler, RepoAdapterV2.OnRepoComponentSelectedListener {

    private RecyclerView recyclerView;
    private RepoAdapterV2 repoAdapterV2;
    private RecyclerView.LayoutManager layoutManager;
    String TAG = ActRepoListing.class.getSimpleName();
    Handler handler = new Handler(Looper.getMainLooper());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_repo_listing);
        initView();
        initListener();
        try {
            initData();
        } catch (WorkerException e) {
            Log.e(TAG, "onCreate: Wrong operational worker - ", e);
        }
    }

    private void initData() throws WorkerException {
        Map<String, String> params = new HashMap<>();
        params.put("rq", "q1");
        HttpUrl link = HttpRequestHandler.getInstance().prepareLink("repositories", params);
        HttpRequestHandler.getInstance().processGet(link, ActRepoListing.this);
        // start the progress indicator
        DialogProgressCustom.getInstance().startProgressDialog(ActRepoListing.this, true);
    }

    private void initListener() {

    }

    private void initView() {
        // recycler view config
        recyclerView = findViewById(R.id.recyclerRepoL);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }


    @Override
    public void onResponseSuccess(ResponseBody response, @Nullable String[] nextLastOrFirstUrl) throws IOException {
        final String responseStr = response.string();
        Log.d(TAG, "onResponseSuccess: " + responseStr);
        Log.d(TAG, "onResponseSuccess-Link URL : " + nextLastOrFirstUrl[0]);
        final List<Repository> repositoryList = extractDataFromResponse(responseStr);
        repoAdapterV2 = new RepoAdapterV2(repositoryList);
        repoAdapterV2.setRepoComponentSelectionListener(this);
        handler.post(new Runnable() {
            @Override
            public void run() {
                recyclerView.setAdapter(repoAdapterV2);
                DialogProgressCustom.getInstance().stopProgressDialog();
            }
        });
    }

    @Override
    public void onResponseError(String errorMessage, IOException e) {
        //todo need to add Notice dialog here
        Log.e(TAG, "onResponseError : " + errorMessage, e);
        //stop progress bar if active
        handler.post(new Runnable() {
            @Override
            public void run() {
                DialogProgressCustom.getInstance().stopProgressDialog();
            }
        });

    }

    private List<Repository> extractDataFromResponse(String serverResponse) {
        List<Repository> repositories = new ArrayList<>();
        try {
            JSONArray rootArray = new JSONArray(serverResponse);
            for (int j = 0; j < rootArray.length(); j++) {
                JSONObject repoSource = rootArray.getJSONObject(j);
                Repository repoSink = new Repository();
                repoSink.setRepoId(repoSource.getInt("id"));
                repoSink.setRepoName(repoSource.getString("name"));
                repoSink.setRepoOwnerName(repoSource.getString("full_name"));
                repoSink.setRepoBranchesLink(repoSource.getString("branches_url"));
                repoSink.setRepoLanguagesLink(repoSource.getString("languages_url"));
                repoSink.setRepoContributorLink(repoSource.getString("contributors_url"));
                repoSink.setRepoDescription(repoSource.getString("description"));
                repoSink.setRepoOwnerAvatarLink(repoSource.getJSONObject("owner").getString("avatar_url"));
                repositories.add(repoSink);
            }
        } catch (JSONException e) {
            Log.e(TAG, "extractDataFromResponse: Parsing error", e);
        }
        Log.d(TAG, "extractDataFromResponse: REPO COUNT : " + repositories.size());
        return repositories;
    }

    /*
     * link RepoAdapterV2 event handler for Contributor selection
     * */

    @Override
    public void onContributorSelected(Repository selectedRepo, int position) {
        Log.d(TAG, "onContributorSelected() called with: selectedRepo Contributor link = [" + selectedRepo.getRepoContributorLink() + "], position = [" + position + "]");
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.fragmentContainer, ContributorsFragment.newInstance(1, selectedRepo.getRepoContributorLink()))
                .commit();
    }

    /*
     * link RepoAdapterV2 event handler for Language selection
     * */
    @Override
    public void onLanguageSelected(Repository selectedRepo, int position) {
        Log.d(TAG, "onLanguageSelected() called with: selectedRepo Language link = [" + selectedRepo.getRepoLanguagesLink() + "], position = [" + position + "]");
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.fragmentContainer, LanguageFragment.newInstance(1, selectedRepo.getRepoLanguagesLink()))
                .commit();
    }

    /*
     * link RepoAdapterV2 event handler for Branch selection
     * */
    @Override
    public void onBranchesSelected(Repository selectedRepo, int position) {
        Log.d(TAG, "onBranchesSelected() called with: selectedRepo Branch link = [" + selectedRepo.getRepoBranchesLink() + "], position = [" + position + "]");
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.fragmentContainer, BranchFragment.newInstance(1, selectedRepo.getRepoBranchesLink()))
                .commit();
    }
}

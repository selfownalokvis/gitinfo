package com.psys.gitinfov2.components.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.psys.gitinfov2.R;
import com.psys.gitinfov2.components.views.beans.Languages;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link com.psys.gitinfov2.components.views.beans.Languages}
 */
public class LanguageRecyclerViewAdapter extends RecyclerView.Adapter<LanguageRecyclerViewAdapter.LanguageViewHolder> {

    private List<Languages> languages;

    public LanguageRecyclerViewAdapter(List<Languages> languages) {
        this.languages = languages;
    }

    @Override
    public LanguageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frag_language, parent, false);
        return new LanguageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LanguageViewHolder holder, int position) {
        holder.mItem = languages.get(position);
        holder.mIdView.setText(languages.get(position).getLanguageName());
        holder.mContentView.setText(languages.get(position).getLanguageCode());

    }

    @Override
    public int getItemCount() {
        return languages.size();
    }

    public class LanguageViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mIdView;
        final TextView mContentView;
        Languages mItem;

        public LanguageViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}

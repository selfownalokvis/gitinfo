package com.psys.gitinfov2.components.views;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.psys.gitinfov2.R;
import com.psys.gitinfov2.components.views.adapters.LanguageRecyclerViewAdapter;
import com.psys.gitinfov2.components.views.beans.Languages;
import com.psys.gitinfov2.support.DialogProgressCustom;
import com.psys.gitinfov2.support.HttpRequestHandler;
import com.psys.gitinfov2.support.WorkerException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.ResponseBody;

/**
 * A fragment representing a list of Language.
 * <p/>
 * Activities containing no interaction callback interface
 */
public class LanguageFragment extends Fragment implements HttpRequestHandler.ResponseHandler {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String ARG_URL_LANGUAGES = "lang_url";
    private int mColumnCount = 1;
    private String languageUrl = "";
    private final String TAG = LanguageFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    Handler handler = new Handler(Looper.getMainLooper());

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public LanguageFragment() {
    }

    public static LanguageFragment newInstance(int columnCount, String url) {
        //todo need to handle null url here
        LanguageFragment fragment = new LanguageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putString(ARG_URL_LANGUAGES, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
            languageUrl = HttpRequestHandler.getInstance().clipExtraUrl(getArguments().getString(ARG_URL_LANGUAGES));

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_language_list, container, false);
        initView(view);
        try {
            initData();
        } catch (WorkerException e) {
            Log.e(TAG, "onCreateView: ", e);
        }
        return view;
    }

    private void initData() throws WorkerException {

        HttpUrl link = HttpUrl.parse(languageUrl);
        HttpRequestHandler.getInstance().processGet(link, this);
        // start the progress indicator
        DialogProgressCustom.getInstance().startProgressDialog(getContext(), true);
    }

    private void initView(View view) {
        // Set the adapter
        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.languageList);
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
    }


    /**
     * <p>Provide result of Request</p>
     *
     * @param response           is content body of response on successful execution of request.
     * @param nextLastOrFirstUrl is array of link URL for pagination support received in header.
     * @throws IOException caught when unexpected event like socket/Connection issue.
     */
    @Override
    public void onResponseSuccess(ResponseBody response, @Nullable String[] nextLastOrFirstUrl) throws IOException {
        final String responseStr = response.string();
        Log.d(TAG, "onResponseSuccess: " + responseStr);
        Log.d(TAG, "onResponseSuccess-Link URL : " + nextLastOrFirstUrl[0]);
        final List<Languages> languages = extractDataFromResponse(responseStr);
        handler.post(new Runnable() {
            @Override
            public void run() {
                recyclerView.setAdapter(new LanguageRecyclerViewAdapter(languages));
                DialogProgressCustom.getInstance().stopProgressDialog();
            }
        });
    }

    /**
     * <p>Provides exception during Request</p>
     *
     * @param errorMessage error message or cause.
     * @param e
     * @throws IOException caught when unexpected event like socket/Connection issue.
     */
    @Override
    public void onResponseError(String errorMessage, IOException e) {
        //todo need to add Notice dialog here
        Log.e(TAG, "onResponseError : " + errorMessage, e);
        //stop progress bar if active
        handler.post(new Runnable() {
            @Override
            public void run() {
                DialogProgressCustom.getInstance().stopProgressDialog();
            }
        });
    }

    private List<Languages> extractDataFromResponse(String serverResponse) {
        List<Languages> languages = new ArrayList<>();
        try {
            JSONObject rootObject = new JSONObject(serverResponse);
            Iterator<String> languagesName = rootObject.keys();
            while (languagesName.hasNext()) {
                Languages language = new Languages();
                String key = languagesName.next();
                language.setLanguageName(key);
                language.setLanguageCode(rootObject.getString(key));
                language.setLanguageIconPath(null);
                languages.add(language);
            }
            if (languages.isEmpty()) {
                Languages language = new Languages();
                language.setLanguageName("no languages defined");
                language.setLanguageCode("0");
                language.setLanguageIconPath(null);
                languages.add(language);
            }
        } catch (JSONException e) {
            Log.e(TAG, "extractDataFromResponse: Parsing error", e);
        }
        Log.d(TAG, "extractDataFromResponse:  COUNT : " + languages.size());
        return languages;
    }
}

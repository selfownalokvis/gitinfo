package com.psys.gitinfov2.components.views.beans;

public class Branch {
    private String branchName;
    private String branchCommitSha;
    private String branchCommitUrl;
    private int totalBranches;

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchCommitSha() {
        return branchCommitSha;
    }

    public void setBranchCommitSha(String branchCommitSha) {
        this.branchCommitSha = branchCommitSha;
    }

    public String getBranchCommitUrl() {
        return branchCommitUrl;
    }

    public void setBranchCommitUrl(String branchCommitUrl) {
        this.branchCommitUrl = branchCommitUrl;
    }

    public int getTotalBranches() {
        return totalBranches;
    }

    public void setTotalBranches(int totalBranches) {
        this.totalBranches = totalBranches;
    }
}

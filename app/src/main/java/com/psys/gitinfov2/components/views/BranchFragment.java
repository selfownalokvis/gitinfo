package com.psys.gitinfov2.components.views;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.psys.gitinfov2.R;
import com.psys.gitinfov2.components.views.adapters.BranchRecyclerViewAdapter;
import com.psys.gitinfov2.components.views.beans.Branch;
import com.psys.gitinfov2.support.DialogProgressCustom;
import com.psys.gitinfov2.support.HttpRequestHandler;
import com.psys.gitinfov2.support.WorkerException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.ResponseBody;

/**
 * A fragment representing a list of Branches.
 * <p/>
 * Activities containing no interaction callback interface
 */
public class BranchFragment extends Fragment implements HttpRequestHandler.ResponseHandler {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String ARG_URL_BRANCHES = "branch_url";
    private int mColumnCount = 1;
    private String branchUrl = "";
    private final String TAG = BranchFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    Handler handler = new Handler(Looper.getMainLooper());

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BranchFragment() {
    }

    public static BranchFragment newInstance(int columnCount, String url) {
        //todo need to handle null url here
        BranchFragment fragment = new BranchFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putString(ARG_URL_BRANCHES, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
            branchUrl = HttpRequestHandler.getInstance().clipExtraUrl(getArguments().getString(ARG_URL_BRANCHES));

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_branch_list, container, false);
        initView(view);
        try {
            initData();
        } catch (WorkerException e) {
            Log.e(TAG, "onCreateView: ", e);
        }
        return view;
    }

    private void initData() throws WorkerException {

        HttpUrl link = HttpUrl.parse(branchUrl);
        HttpRequestHandler.getInstance().processGet(link, this);
        // start the progress indicator
        DialogProgressCustom.getInstance().startProgressDialog(getContext(), true);
    }

    private void initView(View view) {
        // Set the adapter
        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.branchList);
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
    }


    /**
     * <p>Provide result of Request</p>
     *
     * @param response           is content body of response on successful execution of request.
     * @param nextLastOrFirstUrl is array of link URL for pagination support received in header.
     * @throws IOException caught when unexpected event like socket/Connection issue.
     */
    @Override
    public void onResponseSuccess(ResponseBody response, @Nullable String[] nextLastOrFirstUrl) throws IOException {
        final String responseStr = response.string();
        Log.d(TAG, "onResponseSuccess: " + responseStr);
        Log.d(TAG, "onResponseSuccess-Link URL : " + nextLastOrFirstUrl[0]);
        final List<Branch> branches = extractDataFromResponse(responseStr);
        handler.post(new Runnable() {
            @Override
            public void run() {
                recyclerView.setAdapter(new BranchRecyclerViewAdapter(branches));
                DialogProgressCustom.getInstance().stopProgressDialog();
            }
        });
    }

    /**
     * <p>Provides exception during Request</p>
     *
     * @param errorMessage error message or cause.
     * @param e
     * @throws IOException caught when unexpected event like socket/Connection issue.
     */
    @Override
    public void onResponseError(String errorMessage, IOException e) {
        //todo need to add Notice dialog here
        Log.e(TAG, "onResponseError : " + errorMessage, e);
        handler.post(new Runnable() {
            @Override
            public void run() {
                DialogProgressCustom.getInstance().stopProgressDialog();
            }
        });
    }

    private List<Branch> extractDataFromResponse(String serverResponse) {
        List<Branch> branches = new ArrayList<>();
        try {
            JSONArray rootArray = new JSONArray(serverResponse);
            for (int j = 0; j < rootArray.length(); j++) {
                JSONObject branch = rootArray.getJSONObject(j);
                Branch branchObj = new Branch();
                branchObj.setBranchName(branch.getString("name"));
                branchObj.setTotalBranches(rootArray.length());
                branchObj.setBranchCommitSha(branch.getJSONObject("commit").getString("sha"));
                branchObj.setBranchCommitUrl(branch.getJSONObject("commit").getString("url"));
                branches.add(branchObj);
            }
        } catch (JSONException e) {
            Log.e(TAG, "extractDataFromResponse: Parsing error", e);
        }
        Log.d(TAG, "extractDataFromResponse: REPO COUNT : " + branches.size());
        return branches;
    }
}

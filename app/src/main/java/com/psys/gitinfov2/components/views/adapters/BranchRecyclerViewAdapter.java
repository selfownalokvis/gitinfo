package com.psys.gitinfov2.components.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.psys.gitinfov2.R;
import com.psys.gitinfov2.components.views.beans.Branch;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Branch}
 */
public class BranchRecyclerViewAdapter extends RecyclerView.Adapter<BranchRecyclerViewAdapter.BranchViewHolder> {

    private List<Branch> branches;

    public BranchRecyclerViewAdapter(List<Branch> branches) {
        this.branches = branches;
    }

    @Override
    public BranchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frag_branch, parent, false);
        return new BranchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BranchViewHolder holder, int position) {
        holder.mItem = branches.get(position);
        holder.mIdView.setText(branches.get(position).getBranchName());
        holder.mContentView.setText(branches.get(position).getBranchCommitSha());

    }

    @Override
    public int getItemCount() {
        return branches.size();
    }

    public class BranchViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mIdView;
        final TextView mContentView;
        Branch mItem;

        public BranchViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}

package com.psys.gitinfov2.components.views.beans;

import android.support.annotation.Nullable;

public class Languages {
    private String languageName;
    private String languageIconPath;
    private String languageCode;

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    @Nullable
    public String getLanguageIconPath() {
        return languageIconPath;
    }

    public void setLanguageIconPath(String languageIconPath) {
        this.languageIconPath = languageIconPath;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }
}

package com.psys.gitinfov2.components.views.beans;

public class Contributors {
    private String loginName;
    private String avatarUrl;
    private String htmlUrl;
    private int contributions;
    public static final String IMG_THUMB = "thumb";
    public static final String IMG_NORMAL = "normal";

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public int getContributions() {
        return contributions;
    }

    public void setContributions(int contributions) {
        this.contributions = contributions;
    }

    public String getOwnerImageUrlBySize(String type) {
        StringBuilder builder = new StringBuilder();
        switch (type) {
            case IMG_THUMB:
                builder.append(getAvatarUrl());
                builder.append("&s=60");
                break;

            case IMG_NORMAL:
                builder.append(getAvatarUrl());
                break;

            default:
                builder.append(getAvatarUrl());
                builder.append("&s=120");
        }
        return builder.toString();
    }
}
/*
{
    "login": "mojombo",
    "id": 1,
    "avatar_url": "https://avatars0.githubusercontent.com/u/1?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mojombo",
    "html_url": "https://github.com/mojombo",
    "followers_url": "https://api.github.com/users/mojombo/followers",
    "following_url": "https://api.github.com/users/mojombo/following{/other_user}",
    "gists_url": "https://api.github.com/users/mojombo/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mojombo/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mojombo/subscriptions",
    "organizations_url": "https://api.github.com/users/mojombo/orgs",
    "repos_url": "https://api.github.com/users/mojombo/repos",
    "events_url": "https://api.github.com/users/mojombo/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mojombo/received_events",
    "type": "User",
    "site_admin": false,
    "contributions": 177
  }
* */
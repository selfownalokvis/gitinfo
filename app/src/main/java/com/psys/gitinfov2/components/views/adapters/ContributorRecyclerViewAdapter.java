package com.psys.gitinfov2.components.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.psys.gitinfov2.R;
import com.psys.gitinfov2.components.views.beans.Contributors;
import com.psys.gitinfov2.components.views.beans.Repository;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link com.psys.gitinfov2.components.views.beans.Contributors}
 */
public class ContributorRecyclerViewAdapter extends RecyclerView.Adapter<ContributorRecyclerViewAdapter.ContributorViewHolder> {

    private List<Contributors> contributors;

    public ContributorRecyclerViewAdapter(List<Contributors> languages) {
        this.contributors = languages;
    }

    @Override
    public ContributorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frag_contributor, parent, false);
        return new ContributorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ContributorViewHolder holder, int position) {
        holder.mItem = contributors.get(position);
        holder.mIdView.setText(contributors.get(position).getLoginName());
        holder.mContentView.setText(String.valueOf(contributors.get(position).getContributions()));
        Picasso.get().load(contributors.get(position).getOwnerImageUrlBySize(Repository.IMG_THUMB)).into(holder.ivContributorPic);
    }

    @Override
    public int getItemCount() {
        return contributors.size();
    }

    public class ContributorViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mIdView;
        final TextView mContentView;
        Contributors mItem;
        ImageView ivContributorPic;

        public ContributorViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            ivContributorPic = (ImageView) view.findViewById(R.id.ivContributorPic);
            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}

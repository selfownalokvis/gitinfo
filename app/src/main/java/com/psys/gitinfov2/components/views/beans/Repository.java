package com.psys.gitinfov2.components.views.beans;

import android.util.Log;

public class Repository {
    public static final String IMG_THUMB = "thumb";
    public static final String IMG_NORMAL = "normal";
    private String TAG = Repository.class.getSimpleName();
    private int repoId;
    private String repoName;
    private String repoOwnerName;
    private String repoContributorLink;
    private String repoLanguagesLink;
    private String repoBranchesLink;
    private String repoOwnerAvatarLink;
    private String repoDescription;


    public int getRepoId() {
        return repoId;
    }

    public void setRepoId(int repoId) {
        this.repoId = repoId;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getRepoOwnerName() {
        return repoOwnerName;
    }

    public void setRepoOwnerName(String repoOwnerName) {
        this.repoOwnerName = repoOwnerName;
    }

    public String getRepoContributorLink() {
        return repoContributorLink;
    }

    public void setRepoContributorLink(String repoContributorLink) {
        this.repoContributorLink = repoContributorLink;
    }

    public String getRepoLanguagesLink() {
        return repoLanguagesLink;
    }

    public void setRepoLanguagesLink(String repoLanguagesLink) {
        this.repoLanguagesLink = repoLanguagesLink;
    }

    public String getRepoBranchesLink() {
        return repoBranchesLink;
    }

    public void setRepoBranchesLink(String repoBranchesLink) {
        this.repoBranchesLink = repoBranchesLink;
    }

    public String getRepoOwnerAvatarLink() {
        return repoOwnerAvatarLink;
    }

    public void setRepoOwnerAvatarLink(String repoOwnerAvatarLink) {
        this.repoOwnerAvatarLink = repoOwnerAvatarLink;
    }

    public String getRepoDescription() {
        return repoDescription;
    }

    public void setRepoDescription(String repoDescription) {
        this.repoDescription = repoDescription;
    }

    public String getOwnerImageUrlBySize(String type) {
        StringBuilder builder = new StringBuilder();
        switch (type) {
            case IMG_THUMB:
                builder.append(getRepoOwnerAvatarLink());
                builder.append("&s=60");
                break;

            case IMG_NORMAL:
                builder.append(getRepoOwnerAvatarLink());
                break;

            default:
                builder.append(getRepoOwnerAvatarLink());
                builder.append("&s=120");
        }
        return builder.toString();
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        Log.d(TAG, "toString: " + getRepoName());
        return super.toString();
    }
}
